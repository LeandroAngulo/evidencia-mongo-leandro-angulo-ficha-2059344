// usamos la bd creada

use instituto

// creamos la colletion profesores 

db.createCollection("profesores")

// insertamos 5 profesores

db.profesores.insertMany([
    {
        nombre:'Claudia',
        apellido: 'Lopez',
        correo:'LopezClau@gmail.com',
        documento:'60568942',
    },
    {
        nombre:'Fernando',
        apellido: 'Forero',
        correo:'FErcho@gmail.com',
        documento:'15487744',
    },
    {
        nombre:'Melani',
        apellido: 'Pereira',
        correo:'Melani@gmail.com',
        documento:'154547877',
    },
    {
        nombre:'Ana',
        apellido: 'Toima',
        correo:'Anita@gmail.com',
        documento:'54545712',
    },
    {
        nombre:'Gustavo',
        apellido: 'Florez',
        correo:'Gusta@gmail.com',
        documento:'1545748',
    }
])

// creamos la colletion cursos 

db.createCollection("cursos")

// insertamos 8 cursos, con el profesor encargado del curso

db.cursos.insertMany([
    {
        profesor: new ObjectId("5fce6a17c78d5d3387f13341"),
        nombre:'1001',
        creditos: '20',

    },
    {
        profesor: new ObjectId("5fce6a17c78d5d3387f13342"),
        nombre:'1002',
        creditos: '22',
    },
    {
        profesor: new ObjectId("5fce6a17c78d5d3387f13343"),
        nombre:'1003',
        creditos: '30',
    },
    {
        profesor: new ObjectId("5fce6a17c78d5d3387f13344"),
        nombre:'1004',
        creditos: '34',
    },
    {
        profesor: new ObjectId("5fce6a17c78d5d3387f13345"),
        nombre:'1101',
        creditos: '15',
    },
    {
        profesor: new ObjectId("5fce6a17c78d5d3387f13341"),
        nombre:'1102',
        creditos: '26',
    },
    {
        profesor: new ObjectId("5fce6a17c78d5d3387f13342"),
        nombre:'1103',
        creditos: '27',
    },
    {
        profesor: new ObjectId("5fce6a17c78d5d3387f13343"),
        nombre:'1104',
        creditos: '31',
    }
    
])

// creamos la colletion estudiantes 

db.createCollection("estudiantes")

// insertamos 20 estudiantes con el curso al que pertenecen

db.estudiantes.insertMany([
    {
        nombre:'Leandro',
        apellido: 'Angulo',
        correo:'leandro@gmail.com',
        documento:'1000352172',
        curso: new ObjectId("5fce8901c78d5d3387f13364"),

    },
    {
        nombre:'Kevin',
        apellido: 'Ariza',
        correo:'Ariza@gmail.com',
        documento:'1000526784',
        curso: new ObjectId("5fce8901c78d5d3387f13364"),
    },
    {
        nombre:'Mauricio',
        apellido: 'Aros',
        correo:'Mauricio@gmail.com',
        documento:'151214874',
        curso: new ObjectId("5fce8901c78d5d3387f13364"),
    },
    {
        nombre:'Kevin ',
        apellido: 'Castillo',
        correo:'kevin@gmail.com',
        documento:'51564235',
        curso: new ObjectId("5fce8901c78d5d3387f13365"),
    },
    {
        nombre:'juan',
        apellido: 'perez',
        correo:'juanito@gmail.com',
        documento:'345673389',
        curso: new ObjectId("5fce8901c78d5d3387f13365"),
    },
    {
        nombre:'Sandra',
        apellido: 'Ramirez',
        correo:'san@gmail.com',
        documento:'15526456',
        curso: new ObjectId("5fce8901c78d5d3387f13365"),
    },
    {
        nombre:'Laura',
        apellido: 'Torres',
        correo:'torres@gmail.com',
        documento:'151515463',
        curso: new ObjectId("5fce8901c78d5d3387f13366"),
    },
    {
        nombre:'Juliana',
        apellido: 'perez',
        correo:'Juli@gmail.com',
        documento:'3455442789',
        curso: new ObjectId("5fce8901c78d5d3387f13366"),
    },
    {
        nombre:'Sofia',
        apellido: 'gomez',
        correo:'gomez@gmail.com',
        documento:'15165420',
        curso: new ObjectId("5fce8901c78d5d3387f13366"),
    },
    {
        nombre:'Sofia',
        apellido: 'Hernadez',
        correo:'Sofia@gmail.com',
        documento:'9558422',
        curso: new ObjectId("5fce8901c78d5d3387f13367"),
    },
    {
        nombre:'Paula',
        apellido: 'Hernadez',
        correo:'Hernadez@gmail.com',
        documento:'452355412',
        curso: new ObjectId("5fce8901c78d5d3387f1336b"),
    },
    {
        nombre:'Jose',
        apellido: 'Galindo',
        correo:'Galindoqgmail.com',
        documento:'45474236',
        curso: new ObjectId("5fce8901c78d5d3387f13367"),
    },
    {
        nombre:'Jose',
        apellido: 'Martinez',
        correo:'Jose@gmail.com',
        documento:'12154542',
        curso: new ObjectId("5fce8901c78d5d3387f13368"),
    },
    {
        nombre:'Maria',
        apellido: 'Martinez',
        correo:'Martinez15@gmail.com',
        documento:'44653248',
        curso: new ObjectId("5fce8901c78d5d3387f13368"),
    },
    {
        nombre:'Santiago',
        apellido: 'Urrea',
        correo:'Santi@gmail.com',
        documento:'455251599',
        curso: new ObjectId("5fce8901c78d5d3387f1336b"),
    },
    {
        nombre:'Sebastian',
        apellido: 'Velandia',
        correo:'Velandia@gmail.com',
        documento:'78784548',
        curso: new ObjectId("5fce8901c78d5d3387f13369"),
    },
    {
        nombre:'Jaime',
        apellido: 'Lopez',
        correo:'Jaime@gmail.com',
        documento:'545603287',
        curso: new ObjectId("5fce8901c78d5d3387f13369"),
    },
    {
        nombre:'David',
        apellido: 'Medina',
        correo:'David@gmail.com',
        documento:'541465488',
        curso: new ObjectId("5fce8901c78d5d3387f13369"),
    },
    {
        nombre:'Luz',
        apellido: 'Arevalo',
        correo:'Luz15@gmail.com',
        documento:'54548125',
        curso: new ObjectId("5fce8901c78d5d3387f1336a"),
    },
    {
        nombre:'William',
        apellido: 'Forero',
        correo:'Willi@gmail.com',
        documento:'4548125',
        curso: new ObjectId("5fce8901c78d5d3387f1336a"),
    }
])

//listar estudiantes

db.estudiantes.find().pretty()

//listar profesores

db.profesores.find().pretty()

//listar cursos

db.cursos.find().pretty()

// listar cursos de un profesor 

db.profesores.aggregate(
    [
        {
            $match: {
                _id: ObjectId("5fce6a17c78d5d3387f13341")
            }
        },
        {
            $lookup: {
                from:'cursos',
                localField:'_id',
                foreignField:'profesor',
                as:'cursos',

            }
        }

    ]

).pretty()

//listar los estudiantes de un curso

db.cursos.aggregate(
    [
        {
            $match: {
                _id: ObjectId("5fce8901c78d5d3387f13364")
            }
        },
        {
            $lookup: {
                from:'estudiantes',
                localField:'_id',
                foreignField:'curso',
                as:'estudiantes',

            }
        }

    ]

).pretty()

// listar los cursos de un profesor con sus estudiantes

db.profesores.aggregate(
    [
        {
            $match: {
                _id: ObjectId("5fce6a17c78d5d3387f13341")
            }
        },
        {
            $lookup: {
                from:'cursos',
                localField:'_id',
                foreignField:'profesor',
                as:'cursos',

            }
        },
        {
            $unwind: '$cursos'
        },
        {
            $lookup: {
                from: 'estudiantes',
                localField:'cursos._id',
                foreignField: 'curso',
                as: 'estudiantes' 
            }
        }

    ]

).pretty()